package oop3.Fraction;

import java.util.*;

public class Fraction {

    private int numerator;
    private int denumerator;

    public Fraction() {

        numerator = 0;
        denumerator = 1;
    }

    public Fraction(int numerator, int denumerator) {
        this.numerator = numerator;
        this.denumerator = denumerator;
    }

    Scanner input = new Scanner(System.in);

    public void inputData() {
        System.out.println("Nh?p v�o t? s?: ");
        this.numerator = input.nextInt();
        do {
            System.out.println("Nh?p v�o m?u s?: ");
            this.denumerator = input.nextInt();
        }
        while (this.denumerator <= 0);
    }

    public void outPutData(String a) {
        if (this.numerator == 0)
            System.out.println(a + " 0");
        else
            System.out.println(a + " " + this.numerator + "/" + this.denumerator);
    }

    // tim ucln
    public int UCLN(int a, int b) {

        while (a * b != 0) {
            if (a > b) a = a % b;
            else b = b % a;
        }
        return a + b;
    }

    public void toiGian() {

        int UCLN;
        if (this.numerator < 0)
            UCLN = UCLN(-this.numerator, this.denumerator);
        else
            UCLN = UCLN(this.numerator, this.denumerator);
        this.numerator = this.numerator / UCLN;
        this.denumerator = this.denumerator / UCLN;
    }

    public Fraction plus(Fraction phanSo) {

        Fraction fraction = new Fraction();
        fraction.numerator = this.numerator * phanSo.denumerator + this.denumerator * phanSo.numerator;
        fraction.denumerator = this.denumerator * phanSo.denumerator;
        fraction.toiGian();
        return fraction;
    }

    public Fraction minus(Fraction phanSo) {

        Fraction fraction = new Fraction();
        fraction.numerator = this.numerator * phanSo.denumerator - this.denumerator * phanSo.numerator;
        fraction.denumerator = this.denumerator * phanSo.denumerator;
        fraction.toiGian();

        if (fraction.denumerator < 0) {
            fraction.denumerator = -fraction.denumerator;
            fraction.numerator = -fraction.numerator;
        }
        return fraction;
    }

    public Fraction multiply(Fraction phanSo) {

        Fraction fraction = new Fraction();
        fraction.numerator = this.numerator * phanSo.numerator;
        fraction.denumerator = this.denumerator * phanSo.denumerator;
        fraction.toiGian();
        return fraction;
    }

}


