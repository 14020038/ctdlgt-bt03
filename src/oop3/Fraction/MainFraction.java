package oop3.Fraction;

import oop3.Fraction.Fraction;

/**
 * Created by Chung on 9/19/2015.
 */
public class MainFraction {

    public static void main(String[] args) {


        Fraction phanSo1 = new Fraction(12, 39);
        Fraction phanSo2 = new Fraction(13,47);
        phanSo1.toiGian();
        phanSo1.outPutData("Ph�n s? th? nh?t sau khi t?i gi?n l�: ");

        phanSo2.toiGian();
        phanSo2.outPutData("Ph�n s? th? hai sau khi t?i gi?n l�: ");

        phanSo1.plus(phanSo2).outPutData("T?ng hai ph�n s? l�: ");
        phanSo1.minus(phanSo2).outPutData("Hi?u hai ph�n s? l�: ");
        phanSo1.multiply(phanSo2).outPutData("T�ch hai ph�n s? l�: ");
    }
}

